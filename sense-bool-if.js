module.exports = function(RED) 
{
    function CheckVal(config) 
    {
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            if(typeof msg.payload == "boolean")
            {
                switch(config.condition)
                {
                    case "equals":
                        if(msg.payload === (config.checkvalue === "true"))
                            node.send(msg);
                        break;
                    case "notequal":
                        if(msg.payload !== (config.checkvalue === "true"))
                            node.send(msg);
                        break;
                }
            }
        });
    }

    RED.nodes.registerType("bool-if", CheckVal);
}