module.exports = function(RED) 
{
    function CheckVal(config) 
    {
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            if(typeof msg.payload == "number")
            {
                switch(config.condition)
                {
                    case "equals":
                        if(msg.payload === config.checkvalue)
                            node.send(msg);
                        break;
                    case "larger":
                        if(msg.payload > config.checkvalue)
                            node.send(msg);
                        break;
                    case "smaller":
                        if(msg.payload < config.checkvalue)
                            node.send(msg);
                        break;
                    case "notequal":
                        if(msg.payload !== config.checkvalue)
                            node.send(msg);
                        break;
                    case "rounded":
                        if(msg.payload % 1 == 0)
                            node.send(msg);
                        break;
                    case "even":
                        if(msg.payload % 2 === 0)
                            node.send(msg);
                        break;
                    case "odd":
                        if(msg.payload % 2 !== 0)
                            node.send(msg);
                        break;
                }
            }
        });
    }

    RED.nodes.registerType("num-if", CheckVal);
}