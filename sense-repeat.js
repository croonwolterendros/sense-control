module.exports = function(RED) 
{
    function CheckVal(config) 
    {
        RED.nodes.createNode(this, config);
        var node = this;

        node.on('input', function(msg) 
        {
            if(config.amount > 0)
            {
                for(var i = 0; i < config.amount; i++)
                {
                    node.send(msg);
                }
            }
        });
    }

    RED.nodes.registerType("repeat", CheckVal);
}